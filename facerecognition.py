import face_recognition
import sys
import time

#params
credential_image_path = sys.argv[1]
selfie_image_path = sys.argv[2]
start_time = time.time()

credential_image = face_recognition.load_image_file(credential_image_path)
selfie_image = face_recognition.load_image_file(selfie_image_path)

credential_encoding = face_recognition.face_encodings(credential_image)[0]
selfie_encoding = face_recognition.face_encodings(selfie_image)[0]


face_distances = face_recognition.face_distance([credential_encoding], selfie_encoding)
results = face_recognition.compare_faces([credential_encoding], selfie_encoding, tolerance=0.5)
end_time = time.time()

print("are equals: {}".format(results[0]))
print("distance between images: {}".format(face_distances[0]))
print("time execution: {}".format(end_time - start_time))