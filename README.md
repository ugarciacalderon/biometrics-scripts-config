# Script para Reconocimiento Facial

## Configuración y Ejecución

- chmod 777 configuracion.sh
- ./configuracion.sh


## Resultado de Ejecución
![Resultado de Ejecución](image.png)


## Ejecución solo script python

- python facerecognition.py ocr_images/jorgeocr.png selfies/jorge1.png

## Parámetros

- imagen ocr: imagen base de comparación
- imagen selfie imagen selfie a comparar