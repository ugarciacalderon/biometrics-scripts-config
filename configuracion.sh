#!/bin/bash
# configuracion miniconda y entorno virtual
rm -rf ~/miniconda
rm -rf Miniconda3-latest-Linux-x86_64.sh
# download miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

bash Miniconda3-latest-Linux-x86_64.sh -b -u    

source ~/miniconda3/bin/activate

conda init bash
#conda init fish

rm -rf Miniconda3-latest-Linux-x86_64.sh

conda install -c conda-forge dlib -y

yes | pip install cmake
yes | pip install face_recognition

echo "###################################################################### Probando configuracion ######################################################################"
echo "Ejecuta script de prueba"
python facerecognition.py ocr_images/jorgeocr.png selfies/jorge1.png
echo "####################################################################################################################################################################"
