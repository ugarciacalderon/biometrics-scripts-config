boltons @ file:///work/ci_py311/boltons_1677685195580/work
brotlipy==0.7.0
certifi==2023.7.22
cffi @ file:///work/ci_py311/cffi_1676822533496/work
charset-normalizer @ file:///tmp/build/80754af9/charset-normalizer_1630003229654/work
click==8.1.7
cmake==3.27.2
conda @ file:///home/conda/feedstock_root/build_artifacts/conda_1690552936844/work
conda-content-trust @ file:///work/ci_py311/conda-content-trust_1676851327497/work
conda-libmamba-solver @ file:///croot/conda-libmamba-solver_1685032319139/work/src
conda-package-handling @ file:///croot/conda-package-handling_1685024767917/work
conda_package_streaming @ file:///croot/conda-package-streaming_1685019673878/work
cryptography @ file:///croot/cryptography_1686613057838/work
dlib @ file:///home/conda/feedstock_root/build_artifacts/dlib_1685975611752/work
face-recognition==1.3.0
face-recognition-models==0.3.0
idna @ file:///work/ci_py311/idna_1676822698822/work
jsonpatch @ file:///tmp/build/80754af9/jsonpatch_1615747632069/work
jsonpointer==2.1
libmambapy @ file:///croot/mamba-split_1685993156657/work/libmambapy
numpy @ file:///home/conda/feedstock_root/build_artifacts/numpy_1691056235090/work
packaging @ file:///croot/packaging_1678965309396/work
Pillow==10.0.0
pluggy @ file:///work/ci_py311/pluggy_1676822818071/work
pycosat @ file:///work/ci_py311/pycosat_1676838522308/work
pycparser @ file:///tmp/build/80754af9/pycparser_1636541352034/work
pyOpenSSL @ file:///croot/pyopenssl_1678965284384/work
PySocks @ file:///work/ci_py311/pysocks_1676822712504/work
requests @ file:///croot/requests_1682607517574/work
ruamel.yaml @ file:///work/ci_py311/ruamel.yaml_1676838772170/work
six @ file:///tmp/build/80754af9/six_1644875935023/work
toolz @ file:///work/ci_py311/toolz_1676827522705/work
tqdm @ file:///croot/tqdm_1679561862951/work
urllib3 @ file:///croot/urllib3_1686163155763/work
zstandard @ file:///work/ci_py311_2/zstandard_1679339489613/work
